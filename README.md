# INTERVIEW TASK

Require to implement binary search algorithm

# Description

Find index of element in array that equals searched value

# Example:
```
array = { -5, -3, 0, 10, 50}
searchValue = 10
result = 3
```

# NOTE: Algorithm implemented in class BinarySearchSimple
# NOTE: Run BinarySearchSimpleTest for checking