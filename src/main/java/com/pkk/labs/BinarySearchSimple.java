package com.pkk.labs;

public class BinarySearchSimple implements BinarySearch {

    @Override
    public int findIndex(int[] arr, int search) {
        if (arr == null) {
            throw new IllegalArgumentException("array is null");
        }
        if (arr.length == 0) {
            throw new IllegalArgumentException("array is empty");
        }


        int leftIndex = 0;
        int rightIndex = arr.length;
        return binarySearch(arr, search, leftIndex, rightIndex);
    }

    private int binarySearch(int[] arr, int searchValue, int leftIndex, int rightIndex) {
        int middleIndex = leftIndex + (rightIndex - leftIndex) / 2;

        if (arr[middleIndex] == searchValue) {
            return middleIndex;
        }

        if (rightIndex - leftIndex == 1) {
            throw new IllegalArgumentException("array does not contain value = " + searchValue);
        }

        //right part of array
        if (arr[middleIndex] < searchValue) {
            leftIndex = middleIndex;
        } else {
            rightIndex = middleIndex;
        }

        return binarySearch(arr, searchValue, leftIndex, rightIndex);
    }
}
