package com.pkk.labs;

public interface BinarySearch {

    int findIndex(int[] arr, int search);
}
