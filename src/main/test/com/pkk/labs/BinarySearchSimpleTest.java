package com.pkk.labs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.util.stream.Stream;

class BinarySearchSimpleTest {

    private static class SuccessArgumentProvider implements ArgumentsProvider {

        @Override
        public Stream<Arguments> provideArguments(ExtensionContext context) {
            return Stream.of(
                    Arguments.of(new int[]{ -5, -3, 0, 10, 50}, -5, 0),
                    Arguments.of(new int[]{ -5, -3, 0, 10, 50}, -3, 1),
                    Arguments.of(new int[]{ -5, -3, 0, 10, 50}, 0, 2),
                    Arguments.of(new int[]{ -5, -3, 0, 10, 50}, 10, 3),
                    Arguments.of(new int[]{ -5, -3, 0, 10, 50}, 50, 4),

                    Arguments.of(new int[]{ -5, -3, 0, 10}, -5, 0),
                    Arguments.of(new int[]{ -5, -3, 0, 10}, -3, 1),
                    Arguments.of(new int[]{ -5, -3, 0, 10}, 0, 2),
                    Arguments.of(new int[]{ -5, -3, 0, 10}, 10, 3),

                    Arguments.of(new int[]{ -5}, -5, 0)
            );
        }
    }

    @ParameterizedTest(name = "{index}: arguments: {0}, {1}; expected: {2};")
    @ArgumentsSource(SuccessArgumentProvider.class)
    public void find_success(int[] arr, int search, int expected) {
        //GIVEN
        BinarySearch sut = new BinarySearchSimple();

        //WHEN
        int result = sut.findIndex(arr, search);

        //THEN
        assertEquals(expected, result);
    }


    private static class ExceptionArgumentProvider implements ArgumentsProvider {

        @Override
        public Stream<Arguments> provideArguments(ExtensionContext context) {
            return Stream.of(
                    Arguments.of(new int[]{ -5, -3, 0, 10, 50}, 100, "array does not contain value = 100"),
                    Arguments.of(new int[]{ -5, -3, 0, 10, 50}, -50, "array does not contain value = -50"),
                    Arguments.of(null, 0, "array is null"),
                    Arguments.of(new int[]{}, 0, "array is empty")
            );
        }
    }

    @ParameterizedTest(name = "{index}: arguments: {0}, {1}; expected: {2};")
    @ArgumentsSource(ExceptionArgumentProvider.class)
    public void find_fail(int[] arr, int search, String expectedExceptionMessage) {
        //GIVEN
        BinarySearch sut = new BinarySearchSimple();

        //WHEN
        IllegalArgumentException actualException = assertThrows(IllegalArgumentException.class, () -> {
            sut.findIndex(arr, search);
        });

        //THEN
        assertNotNull(actualException);
        assertEquals(expectedExceptionMessage, actualException.getMessage());
    }
}